import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { detailsProduct, updateProduct } from '../actions/productActions';
import LoadingBox from '../components/LoadingBox';
import MessageBox from '../components/MessageBox';
import { PRODUCT_UPDATE_RESET } from '../constants/productConstants';

export default function ProductEdit(props) {
  const productId = props.match.params.id;
  const [name, setName] = useState('');
  const [price, setPrice] = useState('');
  const [image, setImage] = useState('');
  const [category, setCategory] = useState('');
  const [countInStock, setCountInStock] = useState('');
  const [brand, setBrand] = useState('');
  const [description, setDescription] = useState('');
  const [isActive, setIsActive] = useState('');

  const productDetails = useSelector((state) => state.productDetails);
  const { loading, error, product } = productDetails;

  const productUpdate = useSelector((state) => state.productUpdate);
  const {
    loading: loadingUpdate,
    error: errorUpdate,
    success: successUpdate,
  } = productUpdate;

  const dispatch = useDispatch();
  useEffect(() => {
    if (successUpdate) {
      props.history.push('/productlist');
    }
    if (!product || product._id !== productId || successUpdate) {
      dispatch({ type: PRODUCT_UPDATE_RESET });
      dispatch(detailsProduct(productId));
    } else {
      setName(product.name);
      setPrice(product.price);
      setImage(product.image);
      setCategory(product.category);
      setCountInStock(product.stock);
      setBrand(product.brand);
      setDescription(product.description);
      setIsActive(product.isActive);
    }
  }, [product, dispatch, productId, successUpdate, props.history]);
  const submitHandler = (e) => {
    e.preventDefault();
    dispatch(
      updateProduct({
        _id: productId,
        name,
        price,
        image,
        category,
        brand,
        countInStock,
        description,
        isActive,
      })
    );
  };
  return (
    <div>
      <form className="form" onSubmit={submitHandler}>
      
         
        
        {loadingUpdate && <LoadingBox></LoadingBox>}
        {errorUpdate && <MessageBox variant="danger">{errorUpdate}</MessageBox>}
        {loading ? (
          <LoadingBox></LoadingBox>
        ) : error ? (
          <MessageBox variant="danger">{error}</MessageBox>
        ) : (
          <>
            <div className="to-table center">
            <h1>Edit Product <br /> {productId}</h1>
            <div className="to-row">
              <label className="to-cell" htmlFor="name">Name</label>
              <input className="to-cell"
                id="name"
                type="text"
                placeholder="Enter name"
                value={name}
                onChange={(e) => setName(e.target.value)}
              ></input>
            </div>
            <div className="to-row">
              <label className="to-cell" htmlFor="price">Price</label>
              <input className="to-cell"
                id="price"
                type="text"
                placeholder="Enter price"
                value={price}
                onChange={(e) => setPrice(e.target.value)}
              ></input>
            </div>
            <div className="to-row">
              <label className="to-cell" htmlFor="image">Image</label>
              <input className="to-cell"
                id="image"
                type="text"
                placeholder="Enter image"
                value={image}
                onChange={(e) => setImage(e.target.value)}
              ></input>
            </div>
            <div className="to-row">
              <label className="to-cell" htmlFor="category">Category</label>
              <input className="to-cell"
                id="category"
                type="text"
                placeholder="Enter category"
                value={category}
                onChange={(e) => setCategory(e.target.value)}
              ></input>
            </div>
            <div className="to-row">
              <label className="to-cell" htmlFor="brand">Brand</label>
              <input className="to-cell"
                id="brand"
                type="text"
                placeholder="Enter brand"
                value={brand}
                onChange={(e) => setBrand(e.target.value)}
              ></input>
            </div>
            <div className="to-row">
              <label className="to-cell" htmlFor="countInStock">Stock</label>
              <input className="to-cell"
                id="countInStock"
                type="text"
                placeholder="Enter countInStock"
                value={countInStock}
                onChange={(e) => setCountInStock(e.target.value)}
              ></input>
            </div>
            <div className="to-row">
              <label className="to-cell" htmlFor="description">Description</label>
              <textarea className="to-cell"
                id="description"
                rows="3"
                type="text"
                placeholder="Enter description"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
              ></textarea>
            </div>
            <div className="to-row">
              <label className="to-cell">isActive</label>
              {isActive 
              ?<label className="isActive to-cell"
             onClick={(e) => setIsActive(false)}>       
               TRUE
              </label>
              :<label className="isActive to-cell"
              onClick={(e) => setIsActive(true)}>       
                FALSE
               </label>
            
            }
              
            </div>
            <div>
              <label></label>
              <button className="btn-save" type="submit">
                Update
              </button>
            </div>

            </div>
          </>
        )}
      </form>
    </div>
  );
}