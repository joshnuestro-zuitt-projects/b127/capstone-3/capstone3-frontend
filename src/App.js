import React from 'react';
import {BrowserRouter, Route, Link} from 'react-router-dom';
import Item from './pages/Item';
import Home from './pages/Home';
import Cart from './pages/Cart';
import { useDispatch, useSelector } from 'react-redux';
import FindSimilar from './pages/FindSimilar';
import Login from './pages/LogIn';
import { signout } from './actions/userActions.js';
import Register from './pages/Register';
import ShippingAddress from './pages/ShippingAddress.js';
import PaymentMethod from './pages/PaymentMethod';
import PlaceOrder from './pages/PlaceOrder';
import Order from './pages/Order';
import OrderHistory from './pages/OrderHistory';
import Profile from './pages/Profile';
import PrivateRoute from './components/PrivateRoute';
import ProductList from './pages/ProductList';
import AdminRoute from './components/AdminRoute';
import ProductEdit from './pages/ProductEdit';
import OrderList from './pages/OrderList';
// import signout from './actions/userActions';

function App() {

  const cart = useSelector(state => state.cart);
  const { cartItems } = cart;

  const userSignin = useSelector((state) => state.userSignin);
  const { userInfo } = userSignin;
  const dispatch = useDispatch();
  const signoutHandler = () => {
    dispatch(signout());
    window.location.href = "/";
  };

  let isAdmin = false;

  (localStorage.userInfo != undefined)
  ? isAdmin = (JSON.parse(localStorage.userInfo)).isAdmin
  : console.log(localStorage.userInfo)

  return (
    <BrowserRouter>
    <div className="grid-container">

    <header className="header-outer">
        <div className="header-inner responsive-wrapper">
          <div className="header-logo">
            <Link to="/"><img src="../icons/shopee.png" alt="joshopee icon" /></Link>
          </div>
          <nav className="header-navigation">
                
                
                {userInfo ? (
                  <div className="dropdown">
                    <Link to="#">
                      {userInfo.name} <i className="fa fa-caret-down"></i>{' '}
                    </Link>
                    <ul className="dropdown-content">
                      <li>
                        <Link to="/profile">My Account</Link>
                      </li>
                      { !userInfo.isAdmin &&
                      <li>
                        <Link to="/orderhistory">My Purchase</Link>
                      </li>
                      }
                      <li>
                        <Link to="#signout" onClick={signoutHandler}>
                          Logout
                        </Link>
                      </li>
                    </ul>
                  </div>
                ) : (
                  <>
                  <Link to="/register">Sign Up</Link>
                  <Link to="/signin">Login</Link>
                  </>
                )}

                {userInfo && userInfo.isAdmin && (
                  <div className="dropdown">
                    <Link to="#admin">
                      Admin <i className="fa fa-caret-down"></i>
                    </Link>
                    <ul className="dropdown-content">
                      {/* <li>
                        <Link to="/dashboard">Dashboard</Link>
                      </li> */}
                      <li>
                        <Link to="/productlist">Products</Link>
                      </li>
                      <li>
                        <Link to="/orderlist">Orders</Link>
                      </li>
                      {/* <li>
                        <Link to="/userlist">Users</Link>
                      </li> */}
                    </ul>
                  </div>
                )}


                {/* {isAdmin == false && ( */}
                <Link to="/cart"><img width="40%" src="../icons/shopping-cart.png" alt="cart" />
                {cartItems.length>0 &&(
                  <span className="badge">{cartItems.length}</span>
                )}
                </Link>
                {/* )} */}
                
            {/*<button>☰</button>  */}


          </nav>
        </div>
      </header>

    <main>
        <Route path="/cart/:id?" component={Cart}></Route>
        <Route exact path="/product/:id" component={Item}></Route>
        <Route exact path="/product/:id/edit" component={ProductEdit}></Route>
        <Route path="/signin" component={Login}></Route>
        <Route path="/register" component={Register}></Route>
        <Route path="/shipping" component={ShippingAddress}></Route>
        <Route path="/payment" component={PaymentMethod}></Route>
        <Route path="/placeorder" component={PlaceOrder}></Route>
        <Route path="/order/:id" component={Order}></Route>
        <Route path="/orderhistory" component={OrderHistory}></Route>
        <PrivateRoute
            path="/profile"
            component={Profile}
          ></PrivateRoute>
        <AdminRoute
            path="/productlist"
            component={ProductList}
          ></AdminRoute>
        <AdminRoute
            path="/orderlist"
            component={OrderList}
          ></AdminRoute>
        <Route exact path="/" component={Home}></Route>
        <Route path="/match/:category" component={FindSimilar}></Route>
    </main>

    <footer> 
        <div className="cat-container">
            <ul className="cat-col">
                <li>MEN'S</li>
                <li>WOMEN'S</li>
                <li>BABIES & KIDS</li>
                <li>MAKEUP & FRAGRANCES</li>
            </ul>
            <ul className="cat-col">
                <li>HOME & LIVING</li>
                <li>ELECTRONICS</li>
                <li>GROCERIES</li>
                <li>HOBBIES</li>
            </ul>
            <ul className="cat-col">
                <li>HEALTH & PERSONAL CARE</li>
                <li>SPORTS & TRAVEL</li>
                <li>PET CARE</li>
                <li>TOYS, GAMES, & COLLECTIBLES</li>
            </ul>
        </div>

        <hr />
        <span className="footer-content">© 2021 Joshopee: A Shopee Clone by Joshua Nuestro. All Rights Reserved</span>
        <span className="footer-content">Country & Region: Singapore | Indonesia | Taiwan | Thailand | Malaysia | Vietnam | Philippines | Brazil | México | Colombia | Chile</span>
    </footer>

    </div>
    </BrowserRouter>
  );
}

export default App;
